{
 inputs = {
 	nixpkgs.url = "nixpkgs/nixos-unstable";
 	home-manager = {
 	      url = "github:nix-community/home-manager";
 	      inputs.nixpkgs.follows = "nixpkgs";
 	    };
 };


 outputs = { self, nixpkgs, home-manager, ...}:
 	let
 		lib = nixpkgs.lib;
 		system = "aarch64-linux";
 		pkgs = nixpkgs.legacyPackages.${system};
 	in {
 		nixosConfigurations = {
 			nixpi = lib.nixosSystem {
 				inherit system;
 				modules = [ ./configuration.nix ];
 			};
 		};
 		homeConfigurations = { 
 			nixos = home-manager.lib.homeManagerConfiguration {
 		        inherit pkgs;
 		        modules = [ ./home.nix ];
 		      };
 		 };
 	};
	
}
