{ config, pkgs, lib, ... }:
{
  imports = [ ./hardware-configuration.nix ];

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  
  system.stateVersion = "23.05";
  # NixOS wants to enable GRUB by default
  boot.loader.grub.enable = false;
  # Enables the generation of /boot/extlinux/extlinux.conf
  boot.loader.generic-extlinux-compatible.enable = true;
 
  # !!! Set to specific linux kernel version
  boot.kernelPackages = pkgs.linuxPackages_latest;

  networking.hostName = "nixpi";
  # !!! Needed for the virtual console to work on the RPi 3, as the default of 16M doesn't seem to be enough.
  # If X.org behaves weirdly (I only saw the cursor) then try increasing this to 256M.
  # On a Raspberry Pi 4 with 4 GB, you should either disable this parameter or increase to at least 64M if you want the USB ports to work.
  boot.kernelParams = ["cma=256M"];

  zramSwap.enable = true;
    
  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  # !!! Adding a swap file is optional, but strongly recommended!
  swapDevices = [ { device = "/swapfile"; size = 4096; } ];

  # systemPackages
  environment.systemPackages = with pkgs; [ 
    vim curl wget nano bind kubectl helm iptables openvpn
    python3 docker-compose micro neofetch git home-manager];

  programs.zsh.enable = true;
  

  services.resolved.enable = true;
  
  services.openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
  };

  services.openvpn.servers = {
      urioVPN    = { config = '' config /root/nixos/openvpn/uriotv.ovpn''; };
    };
  # virtualisation.docker.enable = true;

  networking.firewall.enable = false;

    # Enabling WIFI
    # wireless.enable = true;
    # wireless.interfaces = [ "wlan0" ];
    # If you want to connect also via WIFI to your router
    # wireless.networks."WIFI-SSID".psk = "wifipass";

  # put your own configuration here, for example ssh keys:
  # users.defaultUserShell = pkgs.zsh;
  users.mutableUsers = true;
  users.groups = {
    nixos = {
      gid = 1000;
      name = "nixos";
    };
  };
  users.users = {
    nixos = {
      uid = 1000;
      home = "/home/nixos";
      name = "nixos";
      group = "nixos";
      shell = pkgs.zsh;
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" ];
    };
  };
  users.extraUsers.root.openssh.authorizedKeys.keys = [
    # This is my public key
     "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJdyKlD7C1afPu+34WerptL1nJZBwiYCLHvjnCbRH2iTqtov6c5U/GF6/a5DfPbTOFe2cVX3Min3dWaNeEzDQKusHsiVWIVB5TphgEwIGrPdH5RIS4ofCN3+bTPnjD341ioHR9dlTQiEY3ozFCS74ItRfan4YWSvFxMN38DylUCkf4zBH2NolHbF1rMLEBaN03IjoTUnKjIxpd3uyOMRaOF+KvosYbeBkIg3JgRGPynVLB9mD1S7LH1dBUv6h0aYEcr9+Jb3QZIfTBndmI2VEkDWG/HhXEXiFEm9LRm+dM2sBliaFhwElLwHrBN0fQrB10PR5zW6YerciUA2QxAd0J"
  ];

  security.sudo.wheelNeedsPassword = false;

  services.avahi = {
      enable = true;
      nssmdns = true;
      publish = {
        enable = true;
        addresses = true;
        domain = true;
        hinfo = true;
        userServices = true;
        workstation = true;
      };
  };
}
